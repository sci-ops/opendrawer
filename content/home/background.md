+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # Do not modify this line!
active = true  # Activate this widget? true/false
weight = 15  # Order that this section will appear.

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Frequently Asked Questions"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  color = "white"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  #image = "headers/bubbles-wide.jpg"  # Name of image in `static/img/`.
  #image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = false

[advanced]
 # Custom CSS. 
 css_style = "padding-top: 20px; padding-bottom: 20px;"
 
 # CSS class.
 css_class = ""
+++

**How is this different from the OSF/Open Science Framework?**

This project differs from putting information in two important ways. Firstly, it is much, much quicker! Secondly, it is sharing very different types of information. OpenDrawer is designed to quickly share information about a project that is truely languishing in the file drawer, and probably not shared on the internet at all. It tells you just a little bit about the project, and is an indication that you could potentially invest some time in sharing content from the project. In contrast, OpenDrawer is just a teaser or an abstract of what information is held.

**What if I don't have ethical approval to share my data?**

For older studies that have been in the file drawer for a while, this might be quite common. However, you might still be able to share estimates of effect size, preliminary findings, summary data, or lessons in relation to what went wrong. So even if you are not able to openly share the raw data, you can still share some information from the project.

**I plan to share content on the OSF or some other platform in the future?**

You are still able to share a brief description here and now. Sometimes, preparing data and content for sharing takes longer than expected, or you might change role and not get around to it. So there is no harm to putting a project on OpenDrawer now, and then adding content to eg OSF later.

# Background

Previous projects to document file-drawered research have often taken up a substantial amount of time, and then may not be often (ever?) accessed. OpenDrawer aims to provide a quick searchable alternative.
The OpenDrawer project was born at the SIPS conference in Rotterdam in 2019, and is a bit delayed in its launch due to well, other things (*cough* Coronavirus) going on in the world.

Our contributors are: 

* James Green, University of Limerick, https://www.ajamesgreen.com/ 
* Gjalt-Jorn Peters, Open Universiteit, the Netherlands, https://behaviorchange.eu/
* Sara Schiavone, UC Davis
* Ilse Pit, Institute of Human Sciences, University of Oxford
* Elena Sixtus, Universität Potsdam
* Cillian McHugh, University of Limerick, https://www.cillianmchugh.com/
* Conor Smithson, Vanderbilt University

