+++
# About/Biography widget.
widget = "about"  # Do not modify this line!
active = false  # Activate this widget? true/false
weight = 20  # Order that this section will appear in.

title = "About"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  color = "white"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  #image = "headers/bubbles-wide.jpg"  # Name of image in `static/img/`.
  #image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = false

[advanced]
 # Custom CSS. 
 css_style = "padding-top: 20px; padding-bottom: 20px;"
 
 # CSS class.
 css_class = ""
+++

Previous projects to document file-drawered research have often taken up a substantial amount of time, and then may not be often (ever?) accessed. OpenDrawer aims to provide a quick searchable alternative.
The OpenDrawer project was born at the SIPS conference in Rotterdam in 2019, and is a bit delayed in its launch due to well, other things (*cough* Coronavirus) going on in the world.

Our contributors are: 

* James Green, University of Limerick, https://www.ajamesgreen.com/ 
* Gjalt-Jorn Peters, Open Universiteit, the Netherlands, https://behaviorchange.eu/
* Sara Schiavone, UC Davis
* Ilse Pit, Institute of Human Sciences, University of Oxford
* Elena Sixtus, Universität Potsdam
* Cillian McHugh, University of Limerick, https://www.cillianmchugh.com/
* Conor Smithson, Vanderbilt University

